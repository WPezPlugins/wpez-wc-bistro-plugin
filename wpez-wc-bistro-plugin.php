<?php
/*
Plugin Name: WPezPlugins: WooCommerce Bistro as a Plugin
Plugin URI: TODO
Description: For all intents and purposes Bistro is a plugin that prevents the use of a proper child theme with Storefront. Not any more :)
Version: 1.0.12 + 0.0.1
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: bistro
*/

// For the sake of KISS, We're going to use this as a global with Automattic's other code.
// Not happy about it, but it will - for now - make ongoing maintenance ez'ier.

$str_plugin_dir_path = untrailingslashit(plugin_dir_url(__FILE__));

// TODO - wrap this in a hook?
require_once 'functions.php';