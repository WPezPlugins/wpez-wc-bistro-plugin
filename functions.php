<?php
/**
 * Bistro engine room
 *
 * @package bistro
 */

/**
 * Set the theme version number as a global variable
 */
$theme          = wp_get_theme( 'bistro' );
$bistro_version = $theme['Version'];

$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Load the individual classes required by this theme
 */
require_once( 'inc/class-bistro.php' );

$new_bistro = new Bistro();

add_filter( 'body_class',                            array( $new_bistro, 'body_classes' ) );
add_action( 'wp_enqueue_scripts',                    array( $new_bistro, 'enqueue_styles' ), 40 ); // After Storefront
add_filter( 'storefront_google_font_families',       array( $new_bistro, 'bistro_fonts' ) );
add_filter( 'woocommerce_breadcrumb_defaults',       array( $new_bistro,'change_breadcrumb_delimiter' ) );

add_filter( 'storefront_woocommerce_args',           array( $new_bistro, 'woocommerce_support' ) );

add_filter( 'storefront_product_categories_args',    array( $new_bistro, 'bistro_homepage_categories' ) );
add_filter( 'storefront_recent_products_args',       array( $new_bistro, 'bistro_homepage_products' ) );
add_filter( 'storefront_featured_products_args',     array( $new_bistro, 'bistro_homepage_products' ) );
add_filter( 'storefront_popular_products_args',      array( $new_bistro, 'bistro_homepage_products' ) );
add_filter( 'storefront_on_sale_products_args',      array( $new_bistro, 'bistro_homepage_products' ) );
add_filter( 'storefront_best_selling_products_args', array( $new_bistro, 'bistro_homepage_products' ) );

require_once( 'inc/class-bistro-customizer.php' );
require_once( 'inc/class-bistro-integrations.php' );
require_once( 'inc/bistro-template-hooks.php' );
require_once( 'inc/bistro-template-functions.php' );
require_once( 'inc/plugged.php' );

/**
 * Do not add custom code / snippets here.
 * While Child Themes are generally recommended for customisations, in this case it is not
 * wise. Modifying this file means that your changes will be lost when an automatic update
 * of this theme is performed. Instead, add your customisations to a plugin such as
 * https://github.com/woothemes/theme-customisations
 */
