## WPezPlugins: WooCommerce Bistro Theme as a Plugin

__For all intents and purposes the Bistro Theme is a plugin that prevents the use of a proper WordPress child theme. Ha! Not any more.__

After a handful of minor tweaks Bistro is now a plugin, and the child theme slot it was taking up unnecessarily is now wide open. 


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### _IMPORTANT_

_This plugin's version is the Bistro's version it forked, a plus sign (+),  and then the plugin's own version. If the Bistro version increments the plugin's version will drop back to 0.0.1 and climb up again from there.__

_That is, the plugin's version is relative to the Bistro version it forks._ 


### THE CHANGES

**1) Added wpez-wc-bisro-plugin.php** 

Aside from the WP plugin header (comment) it contains two lines of code:

-- $str_plugin_dir_path = untrailingslashit(plugin_dir_url(\__FILE\__));

-- require_once 'functions.php';


**2) inc / class-bistro.php**

-- Used $str_plugin_dir_url via a global and replaced the appropriate (theme) functions for enqueue'ing

-- Moved instantiation (new Bistro() at the bottom of the class) to the functions.php

-- Moved the hooks in __construct() to the functions.php as well. 

-- Add this line to method enqueue_styles(); wp_enqueue_style( 'bistro-style', $str_plugin_dir_url . '/style.css', $bistro_version, 'storefront-style' );
  

**3) inc / bistro-cutomizer.php**
  
-- Changed: wp_add_inline_style( 'storefront-child-style', $style ); to wp_add_inline_style( 'bistro-style', $style );



### FAQ

__1 - Why?__

Because Bistro as child theme is pointless. It takes up an important slot that a real child theme can utilize.


__2 - But isn't there a way to filter the template requests?__

Yup. But because you have to do that as a plugin the function get_template_part() isn't going to work. If you have very limited needs in terms of temple changes then you might be able to get away with filtering the temple requests. But why chance it? Use this, get yourself a real child theme, move on and don't look back. 
 

__3 - But I paid for Bistro, now what?__

Well, this isn't an official port / fork, so if you have any Bistro-centric questions you can still ask Automattic. The functionality hasn't changed per se, only how it's implemented. 


 __4 - This plugin is free. Do we still need to buy Bistro?__
 
This plugin is simply an alternate way to add Bistro to your website. It is not intended to be a replacement to Bistro per se. We encourage you to keep supporting Bistro. It's in everyone's best interest to do so.  
 

 __5 - What is the handle of the Bistro stylesheet our enqueue'ing needs to have as a deps?__
 
bistro-style


 __#6 - I'm not a developer, can we hire you?__
 
 Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

- If you want to stick with Bistro as a theme you might be interested in this: https://gitlab.com/WPezPlugins/wpez-wc-templates
 
 
### TODO

- Move hooks outside the classes they're current within.  


### CHANGE LOG

__-- 1.0.12 + 0.0.1__

- INIT - yey! child theme!!



